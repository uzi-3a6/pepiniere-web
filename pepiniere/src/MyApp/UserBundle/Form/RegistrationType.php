<?php

namespace MyApp\UserBundle\Form;

use Gregwar\CaptchaBundle\Type\CaptchaType;
use function Sodium\add;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('prenom')
        ->add('Captcha',CaptchaType::class,array('font'=>'Consolas','as_url'=>true,'reload'=>true,'quality'=>1000,'length'=>4,'invalid_message'=>'Veuillez verifier le code '));


        //1000 ==> for a good quality
     //   invalid message if wrong
    // as_url to activate the reload button ( rene captcha

// add the routing to activate the reload option



       /*     ->add('roles', ChoiceType::class, array(
                'label' => 'Type',
                'choices' => array(
                    'ADMIN' => 'ROLE_ADMIN',
                    'CLIENT' => 'ROLE_CLIENT'
                ),
                'required' => true,
                'multiple' => true,)
        );*/


    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }



    public function getBlockPrefix()
    {
        return 'my_app_bundle_registration_type';
    }
}
