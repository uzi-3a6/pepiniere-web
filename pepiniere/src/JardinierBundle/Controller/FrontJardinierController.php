<?php

namespace JardinierBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FrontJardinierController extends Controller
{
    public function RechercheJardinierAction()
    {

        return $this->render('@Jardinier/Default/recherchejardinier.html.twig');
    }

}
