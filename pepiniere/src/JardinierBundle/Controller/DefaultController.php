<?php

namespace JardinierBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('@Jardinier/Default/index.html.twig');
    }
}
